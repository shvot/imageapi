'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.resolve()
    .then(queryInterface.createTable('images', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      hash_name: {
        type: Sequelize.STRING
      },
      path_relative: {
        type: Sequelize.STRING
      },
      path_absolute: {
        type: Sequelize.STRING
      },
      size: {
        type: Sequelize.INTEGER
      },
      old_size: {
        type: Sequelize.INTEGER
      },
      ext: {
        type: Sequelize.STRING
      },
      mime: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('images');
  }
};
