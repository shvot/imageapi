const express = require("express");
const router = express.Router();

const ImageController = require('../controllers/image');
const origin = require('../middleware/origin');



router.get("/", ImageController.index);
router.post("/", origin.genesis, ImageController.save_image);
router.get("/images/:hash", origin.genesis, ImageController.show_image);




module.exports = router;