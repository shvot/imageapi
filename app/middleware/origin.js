exports.genesis = (req, res, next) => {
    try {
        const header = req.headers.authorization;
        if (!header || header != process.env.HEADER_TOKEN) throw 0;
        else next();

    } catch (error) {
        return res.status(401).json({
            message: 'Usuário não autorizado'
        });
    }
};