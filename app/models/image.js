'use strict';
module.exports = (sequelize, DataTypes) => {
  const image = sequelize.define('image', {
    name: DataTypes.STRING,
    hash_name: DataTypes.STRING,
    path_relative: DataTypes.STRING,
    path_absolute: DataTypes.STRING,
    size: DataTypes.INTEGER,
    ext: DataTypes.STRING,
    mime: DataTypes.STRING,
    old_size: DataTypes.INTEGER
  }, {});
  return image;
};