const formidable = require('formidable');
const fs = require('fs');
const sharp = require('sharp');
const models = require('../models');
const mime = require('mime');


exports.index = (req, res, next) => {
  var date = new Date().toLocaleString("pt-BR", {timeZone: "America/Recife"});
	var env = {"status": 200, "time": date};
	res.json(env);
}

exports.save_image = async (req, res, next) => {

    let form = new formidable.IncomingForm();
    form.parse(req, (err, fields, files) => {
      let FormatImage = '';
      if(req.headers.formatimage) FormatImage = req.headers.formatimage;
      else FormatImage = 'webp';




      let hashname = Math.random().toString(36).substr(2, 15) + Date.now().toString(36) + Math.random().toString(36).substr(2, 15) + Math.random().toString(36).substr(2, 15);
      let imageExtension = FormatImage;
      let oldPath = files.file.path;
      let absPath = `${__dirname}/../../uploads/`;
      let newPath = `${absPath + hashname}.${imageExtension}`;
      let relPath = `${hashname}.${imageExtension}`;
      


      async function compressAndSave (temporaryPath, absolutePath, format) {
        const retorno = {};
        if(FormatImage == 'webp'){
          await sharp(temporaryPath).webp({ lossless: false, force: true })
          .toBuffer()
          .then(bufferImage => {
            fs.writeFileSync(absolutePath, bufferImage);
            const stat = fs.statSync(absolutePath);
            retorno.size = stat.size;
          });
      } 
      
      else if (FormatImage == 'png') {

        await sharp(temporaryPath)
        .png({
          quality: 100,
          chromaSubsampling: '4:4:4',
          force: true
        })
        .toBuffer()
        .then(bufferImage => {
          fs.writeFileSync(absolutePath, bufferImage);
          const stat = fs.statSync(absolutePath);
          retorno.size = stat.size;
        });

      } 
      
      else if (FormatImage == 'jpeg') {
        await sharp(temporaryPath).jpeg({
          quality: 70,
          force: true
        })
        .toBuffer()
        .then(bufferImage => {
          fs.writeFileSync(absolutePath, bufferImage);
          const stat = fs.statSync(absolutePath);
          retorno.size = stat.size;
        });
      }
        
        return retorno.size;
      }
      
      async function getImageDatas (path){
        const type = mime.getType(path);
        return type;
      }

      async function saveInDataBase (name, hash_name, path_relative, path_absolute, size, ext, mime, old_size) {

        const img = {
          name: name,
          hash_name: hash_name,
          path_relative: path_relative,
          path_absolute: path_absolute,
          size: size,
          ext: ext,
          mime: mime,
          old_size: old_size
      };


      models.image.create(img)
        .then(result => {
            var response = result.get({
                plain: true
            });

            res.status(201).json({
                message: "Image saved",
                image_url:"/images/" + img.path_relative,
                old_size: response.old_size,
                new_size: response.size
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err.parent.sqlMessage
            });
        }); 

      }
         
      async function orquestrador() {
        const size = await compressAndSave(oldPath, newPath, FormatImage);
        const type = await getImageDatas(newPath);
        await saveInDataBase(files.file.name, hashname, relPath, absPath, size, imageExtension, type, files.file.size);
      }


     
   orquestrador();
       
  


  
    });
}

exports.show_image = (req, res, next) => {



  models.image.findOne({
    where: {
      path_relative: req.params.hash
    }
})
    .then(response => {
        if (!response) return res.status(404).json({
            message: "Arquivo não existente"
        });

        else {
          res.download(response.path_absolute + response.path_relative);
        }

        
    });
}
